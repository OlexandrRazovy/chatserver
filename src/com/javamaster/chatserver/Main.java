package com.javamaster.chatserver;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Main {

    private final ArrayList<Connection> connections = new ArrayList<>();

    private Main(){
        try {
            FileManager.init(); // ініціалізація менеджера файлів
            // добавляю клієнтів в список connection і запускаю з'єднання
            ServerSocket server = new ServerSocket(9654);
            while(true){
                Socket socket = server.accept();
                Connection c = new Connection(socket);
                connections.add(c);
                c.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class Connection extends Thread {

        Socket s;
        InputStream in;
        OutputStream out;
        BufferedReader br;
        PrintWriter pw;
        String nick = "";
        String password = "";
        String token = "";
        String photo;
        int i1, i2;

        /*
        отримую сокет з конструктора
        створюю вхідний і вихідний потоки
         */
        Connection(Socket s){
            try {
                this.s = s;
                in = s.getInputStream();
                out = s.getOutputStream();
                br = new BufferedReader(new InputStreamReader(in,"utf-8"));
                pw = new PrintWriter(new OutputStreamWriter(out,"utf-8"),true);
                pw.println(i1 = Utils.random());
                pw.println(i2 = Utils.random());
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            try {
                while (true) {
                    /*
                    отримую команду від кліянта і обробляю її
                     */
                    switch (br.readLine()) {
                        case "msg": // якщо повідомлення то відправляю всім учасникам чату
                            String token = Utils.enccode(i1,i2,br.readLine());
                            String msg = Utils.enccode(i1,i2,br.readLine());
                            if (this.token.equals(token)){
                                synchronized (connections) {
                                    for (Connection c:connections) {
                                        if (!c.nick.equals(nick)) {
                                            c.pw.println(Utils.enccode(c.i1,c.i2,nick));
                                            c.pw.println(Utils.enccode(c.i1,c.i2,msg));
                                        }
                                    }
                                }
                            }
                            System.out.println(msg);
                            break;
                        case "log": //якщо запит на вхід то провіраю чи є дозвіл від AuthChecker
                            nick = Utils.enccode(i1,i2,br.readLine());
                            password = Utils.enccode(i1,i2,br.readLine());
                            photo = br.readLine();
                            String photoOld = FileManager.readText(nick);
                            new Thread(() -> {
                                FileManager.saveText(nick,photo);
                            }).start();
                            if (!FileManager.checkPermit(nick)) {
                                close();
                                break;
                            } else {
                                FileManager.delPermit(nick);
                                pw.println(Utils.enccode(i1,i2,this.token = Utils.genToken()));
                                synchronized (connections) {
                                    for (Connection c:connections) {
                                        c.pw.println(Utils.enccode(c.i1,c.i2,"ohranik_vasa"));
                                        c.pw.println(Utils.enccode(c.i1,c.i2,nick + "&log"));
                                    }
                                }
                                if (!photo.equals(photoOld)) {
                                    synchronized (connections) {
                                        for (Connection c:connections) {
                                            c.pw.println(Utils.enccode(c.i1,c.i2,"cupd"));
                                            c.pw.println(Utils.enccode(c.i1,c.i2,nick));
                                            c.pw.println(photo);
                                        }
                                    }
                                }
                            }
                            break;
                        case "reg": // то саме шо з log
                            nick = Utils.enccode(i1,i2,br.readLine());
                            password = Utils.enccode(i1,i2,br.readLine());
                            photo = br.readLine();
                            new Thread(() -> {
                                FileManager.saveText(nick,photo);
                            }).start();
                            if (FileManager.checkPermit(nick)) {
                                FileManager.delPermit(nick);
                                FileManager.register(nick,password);
                                pw.println(Utils.enccode(i1,i2,this.token = Utils.genToken()));
                                synchronized (connections) {
                                    for (Connection c:connections){
                                        c.pw.println(Utils.enccode(c.i1,c.i2,"ohranik_vasa"));
                                        c.pw.println(Utils.enccode(c.i1,c.i2,nick + "&log"));
                                    }
                                }
                            } else close();
                            break;
                        case "exit": // якщо вийшов то відправ. всім шо вийшов
                            synchronized (connections) {
                                for (Connection c:connections) {
                                    c.pw.println(Utils.enccode(c.i1,c.i2,"ohranik_vasa"));
                                    c.pw.println(Utils.enccode(c.i1,c.i2,nick + "&leave"));
                                }
                            }
                            close(); // і закриваю з'єднання
                            return;
                        case "getph":
                            String nick = Utils.enccode(i1,i2,br.readLine());
                            pw.println(FileManager.readText(nick));
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                // якщо помилка то відправляю всім шо вийшов і розриваю з'єднання
                synchronized (connections) {
                    for (Connection c:connections) {
                        c.pw.println(Utils.enccode(c.i1,c.i2,"ohranik_vasa"));
                        c.pw.println(Utils.enccode(c.i1,c.i2,nick + "&leave"));
                    }
                }
                close();
            }
        }

        /*
        закриваю сокет і потоки
         */
        private void close() {
            try {
                s.close();
                in.close();
                out.close();
                br.close();
                pw.close();
            } catch (IOException e) {
                System.err.println("close exception");
            }
        }
    }

    /*
    запускаю перевірку пароля, сервер чату, сервер який відправляє картінки
     */
    public static void main(String[] args) {
        new Thread(AuthChecker::new).start();
        new Main();
        new Thread(CacheUpdater::new).start();
    }
}
