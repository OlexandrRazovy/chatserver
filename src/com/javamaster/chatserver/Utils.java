package com.javamaster.chatserver;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;

/**
 * Created by Саша on 20.12.2016.
 *
 */
class Utils {

    static String genToken() {
        char[] chars = new char[50];
        for (int i = 0;i < chars.length;i++) {
            Random r = new Random();
            int a = r.nextInt(3);
            switch (a) {
                case 0:
                    int b = r.nextInt(10);
                    b = b + 48;
                    chars[i] = (char) b;
                    break;
                case 1:
                    int c = r.nextInt(26);
                    c = c + 65;
                    chars[i] = (char) c;
                    break;
                case 2:
                    int d = r.nextInt(26);
                    d = d + 97;
                    chars[i] = (char) d;
                    break;
            }
        }
        return new String(chars);
    }

    static int random(){
        Random r = new Random();
        int i1 = r.nextInt(3);
        int i2 = r.nextInt(3);
        int i3 = r.nextInt(3);
        int i4 = r.nextInt(4);
        int i5 = r.nextInt(4);
        int out = 0;
        switch (i4){
            case 0:
                switch (i5){
                    case 0:
                        out = i1 + i2 * i3;
                        break;
                    case 1:
                        out = i1 + i2 + i3;
                        break;
                    case 2:
                        out = i1 + i2 + i3;
                        break;
                    case 3:
                        out = i1 + i2 * i3;
                        break;
                }
                break;
            case 1:
                switch (i5){
                    case 0:
                        out = i1 * i2 * i3;
                        break;
                    case 1:
                        out = i1 * i2 * i3;
                        break;
                    case 2:
                        out = i1 * i2 + i3;
                        break;
                    case 3:
                        out = i1 + i2 + i3;
                        break;
                }
                break;
            case 2:
                switch (i5){
                    case 0:
                        out = i1 * i2 + i3;
                        break;
                    case 1:
                        out = i1 + i2 * i3;
                        break;
                    case 2:
                        out = i1 * i2 + i3;
                        break;
                    case 3:
                        out = i1 + i2 * i3;
                        break;
                }
                break;
            case 3:
                switch (i5){
                    case 0:
                        out = i1 + i2 * i3;
                        break;
                    case 1:
                        out = i1 * i2 * i3;
                        break;
                    case 2:
                        out = i1 + i2 + i3;
                        break;
                    case 3:
                        out = i1 + i2 * i3;
                        break;
                }
                break;
        }
        if(out == 0) out++;
        return out;
    }

    static String enccode(int i1, int i2 ,String world) {
        String out = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream(world.getBytes().length);
            ByteArrayInputStream bis = new ByteArrayInputStream(world.getBytes());

            byte[] bytes = new byte[1024];
            int length;
            int i = 0;
            while ((length = bis.read(bytes)) > 0){
                int j = 0;
                while (j < length){
                    bytes[j] = (byte) (bytes[j]^i*i1+i2&0xff);
                    j++;
                    i++;
                }
                bos.write(bytes);
            }
            bos.close();
            bis.close();
            out = new String(bos.toByteArray());
            out = out.substring(0,world.length());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out;
    }
}
