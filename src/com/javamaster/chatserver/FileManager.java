package com.javamaster.chatserver;

import java.io.*;

/**
 * Created by Саша on 20.12.2016.
 *
 * тут зібрані методи для роботи з файлами
 */
class FileManager {

    private final static String DATA_PATH = "data/";

    static void init(){
        if (!new File("data").exists()) new File("data").mkdir();
    }

    static boolean checkPermit(String nick){
        return new File(DATA_PATH + nick + ".permit").exists();
    }

    static void delPermit(String nick){
        new File(DATA_PATH + nick + ".permit").delete();
    }

    static void register(String nick,String password){
        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream(DATA_PATH + nick + ".pass"),true);
            pw.println(password);
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    static void saveText(String name, String text){
        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream(DATA_PATH + name),true);
            pw.println(text);
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    static String readText(String name){
        String string;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(DATA_PATH + name)));
            string = br.readLine();
            br.close();
        }catch (Exception e){
            return "";
        }
        return string;
    }

    /*

    R.I.P.

    public static void saveBytes(String name, byte[] bytes){
        try {
            FileOutputStream fos = new FileOutputStream(DATA_PATH + name);
            fos.write(bytes);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static byte[] readBytes(String name){
        byte[] bytes;
        try {
            FileInputStream fis = new FileInputStream(DATA_PATH + name);
            bytes = IOUtils.toByteArray(fis);
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
            return new byte[]{45};
        }
        return bytes;
    }*/
}
