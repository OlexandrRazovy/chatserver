package com.javamaster.chatserver;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by Саша on 29.12.2016.
 *
 * висилає картінка клієнту
 */
class CacheUpdater {

    CacheUpdater(){
        try {
            ServerSocket server = new ServerSocket(9656);
            while (true){
                Socket socket = server.accept();
                new Thread(() -> {
                    try {
                        BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        PrintWriter pw = new PrintWriter(socket.getOutputStream(),true);
                        try {
                            ArrayList<String> names = new ArrayList<>();
                            String s;
                            while (!(s = br.readLine()).equals("end")){
                                names.add(s);
                            }
                            //ArrayList<String> out = new ArrayList<>();
                            for (int i = 0;i < names.size();i++){
                                //out.add(FileManager.readText(names.get(i)));//TODO тут зробити перевірку на наявність файлаи (бажано)
                                pw.println(FileManager.readText(names.get(i)));
                            }
                            pw.println("end");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        br.close();
                        pw.close();
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
