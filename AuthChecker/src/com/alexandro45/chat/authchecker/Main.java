package com.alexandro45.chat.authchecker;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Саша on 20.12.2016.
 *
 */
public class Main {

    Main(){
        try {
            ServerSocket server = new ServerSocket(9655);
            while (true){
                Socket socket = server.accept();
                new Thread(() -> {
                    try {
                        BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        PrintWriter pw = new PrintWriter(socket.getOutputStream(),true);
                        switch (br.readLine()){
                            case "reg":
                                String name = br.readLine();
                                if(checkUser(name)) pw.println("KO");
                                else{
                                    permit(name);
                                    pw.println("OK");
                                }
                                break;
                            case "log":
                                String nick = br.readLine();
                                String password = br.readLine();
                                if(checkPassword(nick,password)){
                                    permit(nick);
                                    pw.println("OK");
                                    System.out.println("OK");
                                }else{
                                    pw.println("KO");
                                    System.out.println("KO");
                                }
                                break;
                        }
                        br.close();
                        pw.close();
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args){
        new Main();
    }

    private boolean checkUser(String name){
        return new File("data/" + name + ".pass").exists();
    }

    private void permit(String nick){
        try {
            new File("data/" + nick + ".permit").createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean checkPassword(String nick,String password){
        try {
            if(checkUser(nick)){
                System.out.println("user Avaliable");
                return password.equals(new BufferedReader(new InputStreamReader(new FileInputStream("data/" + nick + ".pass"))).readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
